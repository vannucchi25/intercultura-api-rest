package studio.volare.filter;

import studio.volare.security.AuthenticationTokenService;
import studio.volare.security.InvalidAuthenticationTokenException;
import studio.volare.security.RequiredTokenException;

import javax.annotation.Priority;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Provider
@Dependent
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    @Inject
    AuthenticationTokenService authenticationTokenService;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        String authorizationHeader = containerRequestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        if (authorizationHeader != null) {
            String authenticationToken = authorizationHeader;

            if(!handleTokenBasedAuthentication(authenticationToken, containerRequestContext)){
                Map<String, String> msg = new HashMap<>();
                msg.put("error", "Invalid key");
                containerRequestContext.abortWith(Response.status(403).entity(msg).build());
            }

        }else {
            Map<String, String> msg = new HashMap<>();
            msg.put("error", "No authentication key");
            containerRequestContext.abortWith(Response.status(403).entity(msg).build());
        }

    }

    private boolean handleTokenBasedAuthentication(String authenticationToken, ContainerRequestContext containerRequestContext) {
        return authenticationTokenService.parseToken(authenticationToken);
    }
}

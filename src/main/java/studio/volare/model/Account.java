package studio.volare.model;

import javax.json.bind.annotation.JsonbAnnotation;
import javax.json.bind.annotation.JsonbProperty;


public class Account {

    @JsonbProperty("user_id")
    private Integer userId;

    @JsonbProperty("name")
    private String name;

    @JsonbProperty("surname")
    private String surname;

    @JsonbProperty("centro_locale")
    private String centroLocale;

    public Account(){

    }

    public Account(Integer userId, String name, String surname, String centroLocale) {
        this.userId = userId;
        this.name = name;
        this.surname = surname;
        this.centroLocale = centroLocale;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCentroLocale() {
        return centroLocale;
    }

    public void setCentroLocale(String centroLocale) {
        this.centroLocale = centroLocale;
    }
}

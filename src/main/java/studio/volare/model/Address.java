package studio.volare.model;

import javax.json.bind.annotation.JsonbProperty;

public class Address {

    @JsonbProperty("address")
    private String address;

    @JsonbProperty("status")
    private AddressStatus addressStatus;

    public Address(){

    }

    public Address(String address, AddressStatus addressStatus) {
        this.address = address;
        this.addressStatus = addressStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public AddressStatus getAddressStatus() {
        return addressStatus;
    }

    public void setAddressStatus(AddressStatus addressStatus) {
        this.addressStatus = addressStatus;
    }
}

package studio.volare.model;

import javax.json.bind.annotation.JsonbProperty;

public class Forward {


    @JsonbProperty("user_id")
    private Integer userId;

    @JsonbProperty("forwarding_address")
    private String forwardingAddress;

    @JsonbProperty("status")
    private ForwardStatus forwardStatus;

    public Forward(){}

    public Forward(Integer userId, String forwardingAddress) {
        this.userId = userId;
        this.forwardingAddress = forwardingAddress;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getForwardingAddress() {
        return forwardingAddress;
    }

    public void setForwardingAddress(String forwardingAddress) {
        this.forwardingAddress = forwardingAddress;
    }

    public ForwardStatus getForwardStatus() {
        return forwardStatus;
    }

    public void setForwardStatus(ForwardStatus forwardStatus) {
        this.forwardStatus = forwardStatus;
    }
}

package studio.volare.model;

public enum ForwardStatus {
    pending,
    ready
}

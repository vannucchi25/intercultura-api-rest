package studio.volare.resource;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
import studio.volare.model.Account;

import javax.enterprise.context.ApplicationScoped;
import javax.json.Json;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.HashMap;
import java.util.Map;

@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class AccountResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "hello";
    }

    @GET
    @Path("/{userid}")
    public Response getAccountByUserId(@PathParam Integer userid) {


        //TODO: sostituire con google account retrieval
        Account account = new Account(20, "Mario", "Rossi", "centroLocale");

        Map<String, Object> accountFound = new HashMap<>();
        accountFound.put("name", account.getName());
        accountFound.put("estendend_surname", account.getSurname() + " "+ account.getCentroLocale());
        accountFound.put("user_id", account.getUserId());


        return Response.ok(accountFound).build();
    }

    @POST
    @Path("/create")
    public Response createAccount(Account account) {

        boolean created = true;

        Map<String, Object> accountCreatedResponse = new HashMap<>();
        accountCreatedResponse.put("user_id", account.getUserId());
        Jsonb jsonb = JsonbBuilder.create();
        String jsonAccount;

        if(created){
            accountCreatedResponse.put("status", "created");
            jsonAccount = jsonb.toJson(account);
            return Response.ok().status(201).build();
        }else {
            accountCreatedResponse.put("status", "error");
            jsonAccount = jsonb.toJson(account);
            return Response.ok().status(500).build();
        }

    }


    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {

        @Override
        public Response toResponse(Exception exception) {
            int code = 500;
            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }
            return Response.status(code)
                    .entity(Json.createObjectBuilder().add("error", exception.getMessage()).add("code", code).build())
                    .build();
        }

    }
}
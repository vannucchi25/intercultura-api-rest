package studio.volare.resource;

import org.jboss.resteasy.annotations.jaxrs.PathParam;
import studio.volare.model.*;

import javax.enterprise.context.ApplicationScoped;
import javax.json.Json;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/forwards")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
public class ForwardResource {

    @GET
    @Path("/{userid}")
    public Response forwardByUserId(@PathParam Integer userId) {

        //recupera i forward associati all'userId
        List<Forward> forwards = new ArrayList<>();
        forwards.add(new Forward(20,"indirizzo1"));
        forwards.add(new Forward(25,"indirizzo2"));


        //crea una lista di address recuperati da ogni forward e associa uno status secondo una certa logica
        List<Address> addresses = new ArrayList<>();
        for (Forward f:forwards
             ) {
            addresses.add(new Address(f.getForwardingAddress(), AddressStatus.pending));
        }

        //compone la risposta
        Map<String, Object> response = new HashMap<>();
        response.put("status", "status");
        response.put("addresses", addresses);
        response.put("active_forward", "email");
        response.put("user_id", userId);


        return Response.ok(response).build();
    }

    @POST
    @Path("/create")
    public Response createForward(Forward forward) {

        //TODO: sostituire con codice che implementa la logica di settaggo dello status per forward creato
        forward.setForwardStatus(ForwardStatus.ready);

        return Response.ok(forward).status(201).build();
    }

    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {

        @Override
        public Response toResponse(Exception exception) {
            int code = 500;
            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }
            return Response.status(code)
                    .entity(Json.createObjectBuilder().add("error", exception.getMessage()).add("code", code).build())
                    .build();
        }

    }
}

package studio.volare.security;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Properties;

@ApplicationScoped
public class AuthenticationTokenService {

    @ConfigProperty(name = "authentication.key")
    String key;

    public boolean parseToken(String token){
        if(token.equals(key)){
            System.out.println("chiave valida");
            return true;
        }else {

            System.out.println("chiave non valida");
            return false;
        }

    }
}

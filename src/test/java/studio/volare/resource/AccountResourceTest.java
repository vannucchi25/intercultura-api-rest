package studio.volare.resource;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import studio.volare.model.Account;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
public class AccountResourceTest {

    @Test
    public void createAccountTest() {

        Jsonb jsonb = JsonbBuilder.create();

        Map<String, Object> account = new HashMap<>();

        account.put("name", "Lorenzo");
        account.put("surname", "Vannucchi");
        account.put("centro_locale", "centrolocaletest");
        account.put("user_id", 10);

        String jsonAccountPayload = jsonb.toJson(account);

        Map<String, Object> responseCreated = new HashMap<>();

        responseCreated.put("status", "created");
        responseCreated.put("user_id", 10);


        given()
                .accept(ContentType.JSON)
                .header("Authorization","testkey")
                .contentType(ContentType.JSON)
                .body(jsonAccountPayload)
          .when().post("/accounts/create")
          .then()
             .statusCode(201)
                .equals(responseCreated);
    }

    @Test
    public void getAccountById(){

        Map<String, Object> response = new HashMap<>();

        response.put("name", "Lorenzo");
        response.put("surname", "Vannucchi");


        given()
                .accept(ContentType.JSON)
                .header("Authorization","test")
                .contentType(ContentType.JSON)
                .when().post("/accounts/20")
                .then()
                .equals(response);
    }

}
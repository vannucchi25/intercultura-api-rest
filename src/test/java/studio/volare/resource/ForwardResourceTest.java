package studio.volare.resource;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import studio.volare.model.Address;
import studio.volare.model.AddressStatus;
import studio.volare.model.Forward;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class ForwardResourceTest {

    @Test
    public void createForwardTest() {

        Jsonb jsonb = JsonbBuilder.create();

        Map<String, Object> forward = new HashMap<>();

        forward.put("used_id", 10);



        forward.put("forwarding_address", "indirizzo_di_test");


        String jsonForwardPayload = jsonb.toJson(forward);

        Map<String, Object> responseCreated = new HashMap<>();

        responseCreated.put("status", "ready");
        responseCreated.put("user_id", 10);
        responseCreated.put("forwarding_address", "indirizzo_di_test");


        given()
                .accept(ContentType.JSON)
                .header("Authorization","testkey")
                .contentType(ContentType.JSON)
                .body(jsonForwardPayload)
                .when().post("/forwards/create")
                .then()
                .statusCode(201)
                .equals(responseCreated);
    }

    @Test
    public void getForwardsByUserId(){

        //recupera i forward associati all'userId
        List<Forward> forwards = new ArrayList<>();
        forwards.add(new Forward(20,"indirizzo1"));
        forwards.add(new Forward(20,"indirizzo2"));


        //crea una lista di address recuperati da ogni forward e associa uno status secondo una certa logica
        List<Address> addresses = new ArrayList<>();
        for (Forward f:forwards
        ) {
            addresses.add(new Address(f.getForwardingAddress(), AddressStatus.pending));
        }

        //compone la risposta
        Map<String, Object> response = new HashMap<>();
        response.put("status", "status");
        response.put("addresses", addresses);
        response.put("active_forward", "email");
        response.put("user_id", 20);

        given()
                .accept(ContentType.JSON)
                .header("Authorization","testkey")
                .contentType(ContentType.JSON)
                .when().get("/forwards/20")
                .then()
                .statusCode(200)
                .equals(response);

    }
}

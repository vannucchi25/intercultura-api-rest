package studio.volare.resource;

import io.quarkus.test.junit.SubstrateTest;

@SubstrateTest
public class NativeAccountResourceIT extends AccountResourceTest {

    // Execute the same tests but in native mode.
}